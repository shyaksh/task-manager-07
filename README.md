# PROJECT INFO
TASK MANAGER

# DEVELOPER INFO

**NAME:** PAVEL BOKHAN

**E-MAIL:** SHYAKSH@GMAIL.COM

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROJECT BUILD

```bash
mvn clean install
```
# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS
[Yandex.disk](https://yadi.sk/d/H17XZ7zY4bsPOA/JSE-04)